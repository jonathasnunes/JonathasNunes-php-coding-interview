<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;
	private $dogModel;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
		$this->dogModel = new DogModel();
	}
	
	public function getBookings() {
        return $this->bookingData;
    }

	public function addBooking($clientId, $price, $checkInDate, $checkOutDate) {

		$averageDogAge = $this->dogModel->calculateAverageDogAge($clientId);

		if ($averageDogAge < 10 && $averageDogAge > 0) {
			$discount = $price * 0.10;
			$price = $price - $discount;
		}

		$newBooking = [
			'clientid' => $clientId,
			'price' => $price,
			'checkindate' => $checkInDate,
			'checkoutdate' => $checkOutDate,
		];

		$bookings = $this->getBookings();

		$newBooking['id'] = end($bookings)['id'] + 1;
		$bookings[] = $newBooking;

		$this->helper->putJson($bookings, 'bookings');

		return $newBooking;
	}
}