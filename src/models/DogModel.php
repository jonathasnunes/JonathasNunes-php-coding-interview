<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel {

	private $helper;
	private $dogData;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/dogs.json');
		$this->dogData = json_decode($string, true);
	}

	public function getDogs() {
		return $this->dogData;
	}

	public function getDogsByClientId($clientId) {
		$dogs = $this->getDogs();

		$dogsClient = null;
		foreach ($dogs as $dog) {
			if ($dog['clientid'] == $clientId) {
				$dogsClient[] = $dog;
			}
		}
		return $dogsClient;
	}

	public function calculateAverageDogAge($clientId) {
		$dogs = $this->getDogsByClientId($clientId);
		
		if (count($dogs) > 0) {
			$totalAge = 0;
			foreach ($dogs as $dog) {
				$totalAge += $dog['age'];
			}
			return $totalAge / count($dogs);
		}
		else {
			return 0;
		}
	}
}